FROM python

RUN mkdir -p /usr/src/TestHook
 
WORKDIR /usr/src/TestHook
 
ADD ./ /usr/src/TestHook

RUN apt-get update \
    && pip install -r requirements.txt
 
EXPOSE 5001